import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ColDef } from 'ag-grid-community';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {


  ngOnInit(): void {
  }

  columnDefs: ColDef[] = [
    { field: 'id' },
    { field: 'buyOrSell', sortable: true, filter: true },
    { headerName: 'Date', field: 'createdTimestamp', sortable: true, filter: true },
    { field: 'price', sortable: true, filter: true },
    { field: 'statusCode', sortable: true, filter: true },
    { field: 'stockTicker', sortable: true, filter: true },
    { field: 'volume', sortable: true, filter: true },
  ];



rowData: Observable<any[]>;

constructor(private http: HttpClient) {
    this.rowData = this.http.get<any[]>('http://pipeline-predators-pipeline-predators.emeadocker3.conygre.com/api/v1/order');
    
  }


}
